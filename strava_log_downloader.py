# external libraries:
# pip install BeautifulSoup4
# pip install selenium
# pip install webdriver-manager
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
from bs4 import BeautifulSoup
import sys
import re
import os
import os.path
import time
from time import strptime
import json

# constants
STRAVA_LOG_URL = 'https://www.strava.com'
STRAVA_LOG_LOGIN_URL = STRAVA_LOG_URL + "/login"
STRAVA_LOG_WORKOUTS_URL = STRAVA_LOG_URL + "/athlete/training"
STRAVA_LOG_WORKOUT_PAGE_URL = STRAVA_LOG_WORKOUTS_URL + "/activities"

USERNAME_FORM_NAME = 'email'
PASSWORD_FORM_NAME = 'password'
LOGS_HTML_DIR_NAME = "logs_html" # folder where all the logs will be downloaded to as html files
LOGS_TEXT_DIR_NAME = "logs_text" # folder where all the logs will be downloaded to as txt files
STATE_FILE_NAME = "state.json" # file to keep state of where the last run left off
LOG_LINKS_FILE_NAME = "strava_log_links.txt"
DO_NOT_TOUCH_DIR_NAME = "DO_NOT_TOUCH"
LOG_LINKS_FILE_PATH = DO_NOT_TOUCH_DIR_NAME + "/" + LOG_LINKS_FILE_NAME
SAVED_BEGIN_DATE_FILE = "curr_begin_date.txt"
SAVED_BEGIN_DATE_FILE_PATH = DO_NOT_TOUCH_DIR_NAME + "/" + SAVED_BEGIN_DATE_FILE
NUM_LOGS_HX_FILE = "num_total_logs_downloaded.txt"
NUM_LOGS_HX_FILE_PATH = DO_NOT_TOUCH_DIR_NAME + "/" + NUM_LOGS_HX_FILE
LOG_TEXT_FILE = "strava_logs.txt"
LOG_TEXT_FORMATTED_FILE = "strava_logs_{}_to_{}.txt"
LOG_TEXT_FILE_PATH = LOGS_TEXT_DIR_NAME + "/" + LOG_TEXT_FILE
LOG_TEXT_FORMATTED_FILE_PATH = LOGS_TEXT_DIR_NAME + "/" + LOG_TEXT_FORMATTED_FILE


SPACING = 20 # the number dashes between each category (distance, duration, pace...)

def read_settings():

     # read settings from settings file if it exists
    try:
        settings = json.load(open('settings.json'))
        settings = settings['strava']
        settings['timeout'] = int(settings['timeout'])
        if  settings['timeout'] < 0 or  settings['timeout'] == 0:  # if not a positive int print message and ask for input again
            print("WARNING: Invalid timeout value. Defautling to 10 seconds.")
            settings['timeout'] = 10
    except Exception as e:
        print ('Failed to read settings file.')
        print ('Make sure that settings.json is included in the working directory of this script and is formatted properly.')
        print ('See the README in this repo for help.')
        print ('Error: ' + str(e))
        exit(1)

    return settings

def login_driver(driver, email, password, timeout, login_with_google):

    driver.get(STRAVA_LOG_LOGIN_URL)
    
    try:
        if (login_with_google == "1"):
            WebDriverWait(driver,timeout).until(EC.element_to_be_clickable((By.CSS_SELECTOR, "a.google-button"))) 
            driver.find_element_by_css_selector("a.google-button").click()
            WebDriverWait(driver,timeout).until(EC.presence_of_element_located((By.ID, "identifierId")))
            WebDriverWait(driver,timeout).until(EC.element_to_be_clickable((By.ID, "identifierId"))) 
            driver.find_element_by_id("identifierId").send_keys(email)
            WebDriverWait(driver,timeout).until(EC.element_to_be_clickable((By.ID, "identifierNext"))) 
            driver.find_element_by_id("identifierNext").click()
            WebDriverWait(driver,timeout).until(EC.presence_of_element_located((By.NAME, "password"))) 
            WebDriverWait(driver,timeout).until(EC.element_to_be_clickable((By.NAME, "password"))) 
            driver.find_element_by_name("password").send_keys(password)
            WebDriverWait(driver,timeout).until(EC.element_to_be_clickable((By.ID, "passwordNext"))) 
            driver.find_element_by_id("passwordNext").click()
        else:
            driver.find_element_by_id("email").send_keys(email)
            driver.find_element_by_id ("password").send_keys(password)
            driver.find_element_by_id("login-button").click()

        WebDriverWait(driver,timeout).until(EC.title_contains("Dashboard")) 
        WebDriverWait(driver,timeout).until(EC.presence_of_element_located((By.ID, "global-search-bar")))
    except Exception as e:
        driver.quit()
        print()
        print("We're sorry, but something went wrong!")
        print("Are you using the correct credentials? Check your email/password in settings.json.")
        print("If you have a slow internet connection, you might need to use a greater timeout value. You can change this in settings.json.")
        print("OR, if you are logging in using Google, you might want to change the sleep time on LINE 44 of this script. (Google can be hard to work with)")
        print("If you think there is some other issue, send a message to jacob@jyoungs.net with the stack trace along with as many details as possible:")
        print()
        print(e)
        exit(1)

def make_directories():
    # make directories for files on the user's drive if they don't exist
    if not os.path.exists(LOGS_HTML_DIR_NAME):
        os.makedirs(LOGS_HTML_DIR_NAME)

    if not os.path.exists(LOGS_TEXT_DIR_NAME):
        os.makedirs(LOGS_TEXT_DIR_NAME)

    if not os.path.exists(DO_NOT_TOUCH_DIR_NAME):
        os.makedirs(DO_NOT_TOUCH_DIR_NAME)


def get_links_to_workouts(driver,timeout):

    activities = []

    driver.get(STRAVA_LOG_WORKOUTS_URL);
    btn_disabled = False
    while not btn_disabled:

        activities.extend(get_log_links_on_page(driver,timeout))

        # let's go to the next page
        try:
            btn = driver.find_element_by_css_selector("button.next_page") # button to click to go through pages
        except:
            # btn does not exist or is disabled
            btn_disabled = True
            break
        if not btn_disabled:
            btn.click()

    # reverse the links so that we start with the oldest log
    return activities[::-1]

def convert_date(date_str, include_time):
    # convert the date from something like "10:28 AM on Saturday, February 16, 2019" to "2019-02-16"
    date_str = date_str.replace(",", "") # remove commas
    date_split = date_str.split() # [time, PM/AM, "on", dayofweek, month, dayofmonth, year]
    date_split[4] = date_split[4][0:3] # abbreviate month
    month_num = strptime(date_split[4],'%b').tm_mon # convert month to num
    if month_num < 10:
        month_num = "0" + str(month_num)
    day_num = date_split[5]
    if int(day_num) < 10:
        day_num = "0" + str(day_num)
    year = date_split[6]

    if include_time:
        time = date_split[0]
        colon_index = date_split[0].find(":")
        if int(time[0:colon_index]) < 10:
            date_split[0] = "0" + str(date_split[0])
        time_of_day = date_split[0].replace(':', '') + date_split[1]
        date = "{}-{}-{}({})".format(year, month_num, day_num, time_of_day)
    else:
        date = "{}-{}-{}".format(year, month_num, day_num)

    return date

def construct_log_file_name(title, date_str, exercise_type):

    date = convert_date(date_str, True)

    title = re.sub(r'[\\/*?:"<>|]',"", title) # remove invalid characters for filename
    title = deEmojify(title)
    title = title.replace("\n", " ")

    # combine each element into example: "2018-05-26(438PM)_Run_My dank title.html"
    file_name = "{}_({})_{}".format(date, exercise_type, title)
    if len(file_name) > 183: # can't save longer than 183 chars ?
        file_name = file_name[0:183]
    file_name = file_name + ".html"

    return file_name

def save_page(page_content, file_name, year):
    page_content = page_content.prettify()
    if not os.path.exists(LOGS_HTML_DIR_NAME + "/" + year):
        os.makedirs(LOGS_HTML_DIR_NAME + "/" + year)
    path = LOGS_HTML_DIR_NAME + "/" + year + "/" + file_name
    try:
        file = open(path, "w+",  encoding='utf8') 
        file.write(str(page_content))
        file.close()
    except Exception as e:
        print ('Failed to write file to ' + path)
        print ('Error: ' + str(e))
        exit(1)

def build_table_content(page):
    table = "\n"
    categories = []
    values = []
    stats_tag = page.find("ul", {"class" : "inline-stats section"})
    for li in stats_tag.find_all("li"):
        text = li.get_text().strip()
        lines = text.split("\n")
        value = lines[0].strip()
        category = lines[-1].strip()
        if "Distance" in category:
            category = "Distance"
        categories.append(category)
        values.append(value)

    for category in categories:
        table += category.ljust(SPACING)
    table += "\n"
    for value in values:
        table += value.ljust(SPACING)
    table += "\n"
    return table

def build_line(character):
    dashes=""
    for x in range(0,5): # 5 times
        for y in range (1, SPACING+1):
            dashes += character
    return dashes + "\n"

def add_log_to_text_file(all_logs_text_file, log_html_content, link, log_number, title, date, exercise_type):

    log_content = "\n"
    log_content += build_line("-")
    log_content += "#" + str(log_number) + "\n"

    log_content += title + "\n"

    log_content += "Date: " + date + "\n"

    log_content += "Exercise type: " + exercise_type + "\n"

    log_content += "Link: " + link + "\n"

    comments_tag = log_html_content.find("div", {"class": "content"})
    if comments_tag:
        log_content += "Comments: " + "\n\n"
        first_p = True
        for paragraph in comments_tag.findAll("p"):
            if not first_p:
                log_content += "\n"
            for line in paragraph.contents:
                if line.name != "br":
                    if line.name == "a":
                        log_content += line.get_text().strip()
                    else:
                        log_content += line.strip()
                    log_content += "\n"
            first_p = False
    else:
        log_content += "\n"

    table_content = build_table_content(log_html_content)
    log_content += table_content
    
    log_content += build_line("-")

    log_content += "\n"
    log_content += build_line("*")
    all_logs_text_file.write(log_content)

def get_log_links_on_page(driver, timeout):

    activities = []

    # get all links on this page
    WebDriverWait(driver, timeout).until(EC.presence_of_element_located((By.CSS_SELECTOR, "tr.training-activity-row")))
    table = driver.find_element(By.ID, "search-results")
    tbody = table.find_element(By.TAG_NAME, "tbody")

    # maybe try locating by class "training-activity-row"?
    rows = tbody.find_elements(By.TAG_NAME, "tr") # get all of the rows in the table
    for row in rows:
        # this is of hacky and honestly not a good design but it's the only thing that would work smh
        link = row.find_element(By.TAG_NAME, "a") #
        href = link.get_attribute("href")
        if "settings" not in href:
            activities.append(href)
    return activities

def get_workout_page(link, driver, timeout):
    driver.get(link)
    WebDriverWait(driver,timeout).until(EC.presence_of_element_located((By.CSS_SELECTOR, "div.activity-description-container")))
    workout_page = BeautifulSoup(driver.page_source, "lxml")
    return workout_page

def clean_page(workout_page, link):
    
    # make sure all links don't work
    for a in workout_page.findAll('a'):
        del a['href']

    # replace header with link to log
    nav = workout_page.find('header', id="global-header")
    nav.clear()
    wrapper = workout_page.new_tag("p", style="text-align: center; padding-top: 20px")
    link_tag = workout_page.new_tag("a", href=link)
    link_tag.string = link
    wrapper.append(link_tag)
    nav.append(wrapper)

    # show full description
    desc = workout_page.find('div', class_="activity-description")
    if desc is not None:
        desc["style"] = "overflow: visible; max-height: 100%"

    # remove sidebar
    workout_page.find('nav', class_="sidenav").decompose()

    # remove footer
    workout_page.find('footer').decompose()

    # remove the "show more" button if it exists
    show_more_button = workout_page.find('a', class_="minimal button toggle-more")
    if show_more_button is not None:
        show_more_button.decompose()

     # remove scripts
    scripts = workout_page.find_all('script')
    for script in scripts:
        script.decompose()

def deEmojify(inputString):
    returnString = ""
    for character in inputString:
        try:
            character.encode("ascii")
            returnString += character
        except UnicodeEncodeError:
            returnString += ''
    return returnString

def get_title(workout_page):
    title_tag = workout_page.find("h1", {"class": "activity-name"})
    title = title_tag.string.strip()
    return title

def save_begin_date(begin_date):
    begin_date_file = open(SAVED_BEGIN_DATE_FILE_PATH, "w+", encoding='utf8')
    begin_date_file.write(begin_date) # save off this begin date so it can be used next run
    begin_date_file.close() 

def get_saved_begin_date():
    begin_date = ""
    if os.path.exists(SAVED_BEGIN_DATE_FILE_PATH):
        begin_date_file = open(SAVED_BEGIN_DATE_FILE_PATH, "r", encoding='utf8')
        for line in begin_date_file.readlines(): # there should only be one line
            begin_date = line.strip()
        begin_date_file.close()
    return begin_date

def save_num_logs_total_hx(num_logs_total_hx):
    num_logs_hx = open(NUM_LOGS_HX_FILE_PATH, "w+", encoding='utf8')
    num_logs_hx.write(str(num_logs_total_hx))
    num_logs_hx.close() 

def get_num_logs_hx():
    num_logs_total_hx = 0
    if os.path.exists(NUM_LOGS_HX_FILE_PATH):
        num_logs_hx = open(NUM_LOGS_HX_FILE_PATH, "r", encoding='utf8')
        for line in num_logs_hx.readlines(): # there should only be one line
            num_logs_total_hx = int(line.strip())
        num_logs_hx.close()
    return num_logs_total_hx

def process_logs(driver, activities, timeout):

    curr_logs_text_file = open(LOG_TEXT_FILE_PATH, "w", encoding='utf8')

    print("Logs to download: " + str(len(activities)))
    num_logs_that_have_already_been_downloaded = get_num_logs_hx()
    log_num = num_logs_that_have_already_been_downloaded + 1

    while len(activities)>0: # activities goes from oldest to newest
        link = activities[0]
        try:
            workout_page = get_workout_page(link, driver, timeout)
        except Exception as e:
            print('We timed out!')
            workout_page = BeautifulSoup(driver.page_source, "lxml")
            pre = workout_page.find("pre")
            if (pre is not None) and ("Too Many Requests" in pre.get_text()):
                print("Unfortunately strava limits the number of requests we can execute in a certain time period, so you're going to have to come back to this later.")
            else:
                 print("An unexpected error occurred.")
            print("Thankfully, we have saved off your progress. Sometime later, just run this script again and we will continue where this left off.")
            curr_logs_text_file.close() 
            return
        else:
            workout_page = BeautifulSoup(driver.page_source, "lxml")

        log_html_content = workout_page.find("section", {"id" : "heading"})

        title = get_title(log_html_content)

        date_tag = log_html_content.find("time")
        date = date_tag.string.strip()
        
        header_tag = log_html_content.find("span", {"class": "title"})
        header = header_tag.get_text().strip()
        exercise_type = header.split("\n")[-1].strip()

        add_log_to_text_file(curr_logs_text_file, log_html_content, link, log_num, title, date, exercise_type)

        # if this log num was a mutliple of 500, close the text file, rename it, and open a new one
        file_date = convert_date(date, False)
        curr_logs_text_file = close_and_reopen_log_text_file_as_approp(curr_logs_text_file, file_date, log_num)
        
        # fix up some things on the log page
        clean_page(workout_page, link)

        # save a link to this log page on the user's drive
        year = file_date[0:file_date.find("-")]
        file_name = construct_log_file_name(title, date, exercise_type)
        save_page(workout_page, file_name, year)

        # remove link from our list
        del activities[0] 

        print("Processed log (" +  str(log_num) + "): " + title)
        log_num += 1

    # if we get here, we have processed all the logs in our log links file!
    curr_logs_text_file.close() 
    driver.quit()

def close_and_reopen_log_text_file_as_approp(curr_logs_text_file, file_date, log_num):
    logs_per_file = 500
    if (log_num % logs_per_file) == 0:
        curr_logs_text_file.close()
        begin_date = get_saved_begin_date()
        end_date = file_date
        os.rename(LOG_TEXT_FILE_PATH, LOG_TEXT_FORMATTED_FILE_PATH.format(begin_date, end_date))
        curr_logs_text_file = open(LOG_TEXT_FILE_PATH, "w", encoding='utf8')
    elif ((log_num-1) % logs_per_file) == 0:
        begin_date = file_date
        save_begin_date(begin_date)
    return curr_logs_text_file

def write_links_to_file(activities):
    links_file = open(LOG_LINKS_FILE_PATH, "w", encoding='utf8')
    for link in activities:
        links_file.write(link + "\n")
    links_file.close() 

def get_links_to_workouts_from_file():
    links_file = open(LOG_LINKS_FILE_PATH, "r", encoding='utf8')
    activities = []
    lines = links_file.readlines() 
    for line in lines:
        activities.append(line.strip())
    links_file.close()
    return activities

def update_log_link_list(activities):
    if (len(activities)>0):
        links_file = open(LOG_LINKS_FILE_PATH, "w", encoding='utf8')
        for link in activities:
            links_file.write(link + "\n")
        links_file.close() 
    else:
        # if there are no activities left, delete the log links file.
        os.remove(LOG_LINKS_FILE_PATH)



def main():

    # start timer. interesting to see how long the script runs.
    start_time = time.time()

    # get settings
    settings = read_settings()

    # make directories for better organization of the files
    make_directories()

    # setup driver and login to strava
    # have to use  selenium since this website is run by js
    driver = webdriver.Chrome(ChromeDriverManager().install())
    login_driver(driver, settings['email'], settings['password'], settings['timeout'], settings['login_with_google'])

    # get number of logs that we have saved all time
    num_logs_total_hx = get_num_logs_hx()


    if os.path.exists(LOG_LINKS_FILE_PATH):
        activities = get_links_to_workouts_from_file()
    else:    
        # grab links to the logs
        activities = get_links_to_workouts(driver, settings['timeout']) # activities is an array of links to all the logs to download

        # remove all the logs in the activities array that we have already downloaded
        i = 0
        while i < num_logs_total_hx:
            del activities[i]
            i += 1

        # save off all the links, so that if this run fails we can get them for a subsequent run
        write_links_to_file(activities)

    num_logs_to_download = len(activities)

    # loop through all of the links and download them, until we get a timeout
    # every log that we successfully download gets deleted from activities
    process_logs(driver, activities, settings['timeout'])
    driver.quit()

    # save off our progress by overwriting our text file of log links with the log links that we were unable to get to in this run
    update_log_link_list(activities)

    # save off running count of logs that have been downloaded
    num_logs_downloaded = num_logs_to_download-len(activities)
    num_logs_total_hx = num_logs_total_hx + num_logs_downloaded # add together the number of logs already downloaded and the logs downloaded from this run
    save_num_logs_total_hx(num_logs_total_hx)

    print()
    print("Num logs downloaded: {}".format(num_logs_downloaded))
    total_time = int(time.time() - start_time)

    import datetime
    print("Time elapsed: {} ".format(str(datetime.timedelta(seconds=total_time))))

    if not os.path.exists(LOG_LINKS_FILE_PATH):
        print("all logs have been successfully downloaded. run this utility again to download any new logs you have uploaded to strava since the most recent date saved in the logs_html folder.")

if __name__ == '__main__':
    main()
