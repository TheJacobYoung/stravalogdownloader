StravaLogDownloader

This is a Python tool for downloading all your logs on strava.com.

Requirements:
1. Have python3 installed on your machine. You can do that here: https://www.python.org/downloads/
2. Install the "BeautifulSoup" python library. This can be done by running the command "pip install BeautifulSoup4" on your command line.
3. Install the "selenium" python library. This can be done by running the command "pip install selenium" on your command line.
4. Install the "webdriver-manager" python library. This can be done by running the command "pip install webdriver-manager" on your command line. This script uses Chrome to download all your logs. This "webdriver-manager" library should take care of that if you don't have Chrome installed, but if it doesn't work you might just need to install Chrome.
5. Have an account on strava.com

It's also probably good to have a strong internet connection. I don't feel like doing extensive testing on making sure that this script works even on slow connections - it might work, it might not (It works on my machine!). But it's 2019. Get yo self some good internet. 

How to use:
1. Download the files from this repository called "strava_log_downloader.py" and "settings.json" into the directory where you want all your logs saved. This can easily be done by click the Downloads icon on the left of this screen (third from bottom in the middle group) and clicking Download repository.
2. Browse to the directory where you downloaded these files and open the settings.json file. You can do this with any text editor (such as notepad, notepad++, sublime text, etc.). Replace "your-username" and "your-password" with your email and password credentials for strava.com, such as "thotdestroyer@lilpump.com" and "donttrustthots". Or, if you are logging in with Google, set this to your Google email and password. Then, open a terminal in this directory and run the command "python strava_log_downloader.py". If you don't know how to do that, this is an easy way for Windows:
3. Press the Windows key
4. Search for "cmd" and open the Command Prompt
5. Copy the path to your strava log downloader directory (something like C:\Users\jacob\Documents\StravaLogDownloader). One way to get this is to right click on the strava_log_downloader.py file, click Properties, and then copy the Location.
6. Enter the following command: cd <your copied location>
   Example: cd C:\Users\jacob\Documents\StravaLogDownloader
7. Finally, enter the command to run the script: python strava_log_downloader.py
8. Watch the magic happen
If you get this error: "Message: session not created: This version of ChromeDriver only supports Chrome version XX", then you just need to update your Chrome browser. You can do this by opening Chrome and going to Help -> About Google Chrome.

UPDATE 2/21/21:
I discovered that this Strava has a limit on the amountof web requests you can execute with a utility per day. it's somewhere around 100.
therefore, to download all your logs, this utility needs to be run multiple days.
i have implemented a way that saves off your progress from each run, so all you should have to do is simply run the script and we will continue downloading logs from where you last left.
to make this work correctly, DO NOT TOUCH the files in the DO NOT TOUCH folder.
If you delete any logs in strava after running the utility, subsequent runs of the utility probably won't work properly. so don't do that.
If you delete any logs in strava, to make this work properly again, you need to reset and download everything. To do that, delete the logs_html, logs_text, and DO NOT TOUCH folders.


If you use a Mac, you're on your own for figuring out how to do this. Or trying following these instructions from Austin:
MAC USERS/PPL WHO HAVE NEVER USED PYTHON BEFORE/DON'T KNOW WHAT A TERMINAL IS:

Okay, buckle up. If you have no previous python experience and also use a Mac, then welcome to the tutorial for you.

First off, download python in step 1. Make sure that you choose the version that starts with 3. After that, you're going to run into some issues. 

The command line that Jacob mentions is called "terminal" on Mac. Open spotlight(either hit CMD+Space, or click the magnifying glass in the upper right hand corner of the menu bar) and launch it. 

Now, for the second step that Jacob put down, copy and paste this(WITHOUT the quotation marks, obviously). "python3 -m pip install BeautifulSoup4". This should trigger a bunch of stuff to happen that frankly you don't care about. Congrats.

For the next step, keep terminal open, but this time type, "python3 -m pip install selenium". Again, you should be seeing a bunch of stuff loading on the screen. Do the same for "python3 -m pip install webdriver-manager".

Alright, now we're going to settings. Go to Keyboard -> shortcuts -> services(found on the left side menu), and then scroll down on the right and check the box next to, "New Terminal at Folder". 

At this point you need to move the file called "strava_log_downloader.py" that you downloaded from Jacob to wherever you want the logs to be. Make sure it's in a folder. 

Now, right click on the folder with the file in it. At the bottom click, "New Terminal at Folder". This will in turn open a terminal window(duh). Now type, "python3 strava_log_downloader.py".

If that didn't work, hit me up on twitter @alanglinais7 and I might help you if you have enough clout.


WHAT HAPPENS:
This script will open Chrome to go through your log history and download every log that you have posted - starting with your oldest ones - as html files. You will see in a file explorer folders created called "logs_html" and "logs_text". The logs_html folder contains html copies of your individual strava logs. You can simply open any of the files in your favorite web browser for viewing. Note that for the pages to display properly, you need to have an internet connection. I just really didn't feel like going through the process of downloading all the css files lol.

File names for the html logs follow the following format: "DATE(TIME)_(Exercise Type)_TITLE.html". Example: "2017-03-05(0638AM)_(Run)_Attempt to run through injury.html". The date is in the form YYYY-MM-DD. Additionally, all invalid file name characters are removed from log titles.

The logs_text folder contains text files that have compiled the logs into a more concise format for viewing and searching.

A text file will also be created that holds links to all of your logs.

SETTINGS
In the settings.json file there are settings that can be configured.
login_with_google: set to 1 to login using your Google email and password. It is less reliable to do this (it works on MY machine, but no guarantees about yours), so it is recommended that you login using your Strava email and password.
timeout: this is the maximum time (in seconds) we wait when loading a page. Needs to be a positive integer.
logs_per_file: maximum number of logs to be compiled into each text file. 

Please send all issues and enhancement ideas directly to jacob@jyoungs.net :)
If you use a Mac I won't help you. Send a message to austin.b.langlinais@gmail.com for that instead.